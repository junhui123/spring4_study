package jpa.sample.business.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;

@Entity
@NamedNativeQuery(
		name="getPetPrice",
		query="exec CALC_PET_PRICE(?)",
		resultClass=Pet.class
)
public class Pet {
	@Id
	private Integer petId;
	private String petName;
	@ManyToOne
	@JoinColumn(name = "owner_id")
	private Owner owner;
	private Integer price;
	private Date birthDate;

	public Integer getPetId() {
		return petId;
	}
	public void setPetId(Integer petId) {
		this.petId = petId;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Date getBirthData() {
		return birthDate;
	}
	public void setBirthData(Date birthDate) {
		this.birthDate = birthDate;
	}
}
