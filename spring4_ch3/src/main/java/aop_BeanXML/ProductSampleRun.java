package aop_BeanXML;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import aop_BeanXML.aop.business.domain.Product;
import aop_BeanXML.aop.business.service.ProductService;

public class ProductSampleRun {

	public static void main(String[] args) {
		ProductSampleRun productSampleRun = new ProductSampleRun();
		productSampleRun.execute();
	}

	@SuppressWarnings("resource")
	public void execute() {
		BeanFactory ctx = new ClassPathXmlApplicationContext("aop_BeanXML/config/applicationContext.xml");
		ProductService productService = ctx.getBean(ProductService.class);

		productService.addProduct(new Product("공책", 100));

		Product product = productService.findByProductName("공책");
		System.out.println(product);
		
		productService.findProductThrowsEx();
	}
}