package di_sample4.di.business.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;

import di_sample4.di.business.domain.Product;

public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Override
	public void addProduct(Product product) {
		productDao.addProduct(product);
	}

	@Override
	public Product findByProductName(String name) {
		return productDao.findByProductName(name);
	}
	
	@PostConstruct
	public void start() {
		System.out.println("@PostConstruct : ProductServiceImpl");
	}
	
	@PreDestroy
	public void stop() {
		System.out.println("@PreDestory : ProductServiceImpl");
	}
}
