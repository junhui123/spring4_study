package sample.Exception;

public class BarException extends Exception {
	private static final long serialVersionUID = 3614922944608509308L;

	public BarException() {
	}

	public BarException(String msg) {
		super(msg);
	}

	public BarException(Throwable th) {
		super(th);
	}

	public BarException(String msg, Throwable th) {
		super(msg, th);
	}
}
