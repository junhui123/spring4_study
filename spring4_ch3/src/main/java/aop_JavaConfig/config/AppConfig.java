package aop_JavaConfig.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import aop_JavaConfig.aop.MyFirstAspect;
import aop_JavaConfig.aop.business.service.ProductServiceImpl;
import aop_JavaConfig.aop.dataaccess.ProductDaoImpl;


@Configuration
//CGLIB proxy
//@EnableAspectJAutoProxy(proxyTargetClass=true)

//JDK dynamic proxy
//@EnableAspectJAutoProxy(proxyTargetClass=false)
@EnableAspectJAutoProxy()
public class AppConfig {
	@Bean
	public ProductServiceImpl productService() {
		return new ProductServiceImpl();
	}

	@Bean
	public ProductDaoImpl productDao() {
		return new ProductDaoImpl();
	}

	@Bean
	public MyFirstAspect myFirstAspect() {
		return new MyFirstAspect();
	}
}
