package MessageSource;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ResourceBundleMessageSource;

public class MessageSource_useXML {
	public static void main(String[] args) {
		useXML();
	}
	
	public static void useXML() {
		ApplicationContext context = new ClassPathXmlApplicationContext("/MessageSource/config/ApplicationContext.xml");
		String msg_default = context.getMessage("geeting", null, Locale.getDefault());
		System.out.println(msg_default);
		String msg_ko = context.getMessage("geeting", null, Locale.KOREA);
		System.out.println(msg_ko);
		String msg_en = context.getMessage("geeting", null, Locale.ENGLISH);
		System.out.println(msg_en);		
	}
}
