package di_sample4.config;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import di_sample4.di.business.service.ProductDao;
import di_sample4.di.business.service.ProductServiceImpl;
import di_sample4.di.dataaccess.ProductDaoImpl;

@Configuration
public class AppConfig {

	//Autowired를 활용한 DI
	@Autowired
	private ProductDao productDao;
	
	@Bean
	public ProductDaoImpl productDao() {
		return new ProductDaoImpl();
	}
	
	@Bean(autowire = Autowire.BY_NAME)
	public ProductServiceImpl productServices() {
		return new ProductServiceImpl();
	}
	
//	@Bean
//	public ProductServiceImpl productServices(ProductDao productDao) {
//		//Setter 인젝션을 이용한 경우
//		return new ProductServiceImpl(productDao);
//	}
	
//	@Bean
//	public ProductServiceImpl productServices() {
//		//Setter 인젝션을 이용한 경우
//		return new ProductServiceImpl(productDao);
//	}
	
//	@Bean
//	public ProductServiceImpl productServices() {
//		//@Bean 메소드를 이용한 결과를 통해 인젝션
//		return new ProductServiceImpl(productDao());
//	}
	
	@PostConstruct
	public void start() {
		System.out.println("@PostConstruct : AppConfig");
	}
	
	@PreDestroy
	public void stop() {
		System.out.println("@PreDestory : AppConfig");
	}
}
