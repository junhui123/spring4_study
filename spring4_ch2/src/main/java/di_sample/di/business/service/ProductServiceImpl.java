package di_sample.di.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import di_sample.di.business.domain.Product;
import di_sample.di.business.service.ProductDao;
import di_sample.di.business.service.ProductService;

//Bean 정의(XML)에서 클래스 스캔 범위 지정
@Component 
public class ProductServiceImpl implements ProductService {

	/**
	 * @Autowired
	 * 	DI 컨테이너가 @Component로 정의된 클래스 중 찾아 인스턴스 주입
	 * 	인젝션을 받기 위한 설정
	 * 	private도 주입 가능 
	 */
	@Autowired 
	private ProductDao productDao;
	
	@Override
	public void addProduct(Product product) {
		productDao.addProduct(product);
	}

	@Override
	public Product findByProductName(String name) {
		return productDao.findByProductName(name);
	}
}
