package aop_JavaConfig;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import aop_JavaConfig.aop.business.domain.Product;
import aop_JavaConfig.aop.business.service.ProductService;
import aop_JavaConfig.config.AppConfig;

public class ProductSampleRun {

	public static void main(String[] args) {
		ProductSampleRun productSampleRun = new ProductSampleRun();
		productSampleRun.execute();
	}

	@SuppressWarnings("resource")
	public void execute() {
		BeanFactory ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		ProductService productService = ctx.getBean(ProductService.class);
		productService.addProduct(new Product("공책", 100));
		Product product = productService.findByProductName("공책");
		System.out.println(product);
	}
}