package transaction;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import transaction.business.domain.Pet;
import transaction.business.service.PetService;

public class TranByProgrammaticMain {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("transaction/config/spring-tranByProgrammatic.xml");
		PetService service = ctx.getBean(PetService.class);
		service.findPetByOwner("홍길동");
		
		Pet pet = new Pet();
		pet.setPetId(1);
		pet.setPetName("나비");
		pet.setOwnerName("홍길동");
		pet.setPrice(36000);
		pet.setBirthDate(new Date());
		service.updatePetProgrammaticTransaction2(pet);
		
		System.out.println();
		service.findPetByOwner("홍길동");
	}
}
