package sample.Exception;

public class SystemException extends Exception {
	private static final long serialVersionUID = 3614922944608509308L;

	public SystemException() {
	}

	public SystemException(String msg) {
		super(msg);
	}

	public SystemException(Throwable th) {
		super(th);
	}

	public SystemException(String msg, Throwable th) {
		super(msg, th);
	}
}
