package transaction.business.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import transaction.business.dao.PetDao;
import transaction.business.domain.Pet;
import transaction.business.exception.BussinessException;
import transaction.business.service.PetService;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDao petDao;
	
	@Autowired
	private PlatformTransactionManager txManager;

	//Bean 정의에 트랜잭션 정의 정보를 설정한 경우는 필요없음
	@Transactional(
			propagation=Propagation.REQUIRED,
			isolation=Isolation.READ_COMMITTED,
			timeout=10,
			readOnly=false,
			rollbackFor=BussinessException.class)
	@Override
	public void updatePet(Pet pet) throws BussinessException {
		petDao.updatePet(pet);
	}

	@Override
	public void updatePetProgrammaticTransaction(Pet pet) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		def.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		def.setTimeout(10);
		def.setReadOnly(false);
		
		TransactionStatus status = txManager.getTransaction(def);
		try {
			petDao.updatePet(pet);
		} catch(RuntimeException e) {
			txManager.rollback(status);
			throw e;
		}
		txManager.commit(status);
	}

	@Override
	public void updatePetProgrammaticTransaction2(final Pet pet) {
		TransactionTemplate t = new TransactionTemplate(txManager);
		t.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		t.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		t.setTimeout(10);
		t.setReadOnly(false);

		t.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				petDao.updatePet(pet);
			}
		});
	}
	
	@Override
	public void findPetByOwner(String name) {
		petDao.findByOwnerName(name);
	}
	
	
}
