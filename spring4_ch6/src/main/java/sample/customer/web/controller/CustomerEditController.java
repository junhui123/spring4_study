package sample.customer.web.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sample.customer.biz.domain.Customer;
import sample.customer.biz.service.CustomerService;
import sample.customer.biz.service.DataNotFoundException;

@Controller
@RequestMapping("/customer/{customerId}")

//값을 유지해야하거나 사용자의 입력이 여러 단계 걸쳐 완료되는 폼의 경우 사용. 이전 정보 재입력폼에서도 사용
@SessionAttributes(value="editCustomer")
public class CustomerEditController {

	//미입력의 경우 공백 문자가 아닌 null이 설정
/*	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}*/
	
	@Autowired
	private CustomerService customerService;
	
	//@PathVariable = {}안의 지정된 변수명(변동이 있는 값)과 메소드 인수명을 같게하여 변동이 있는 값을 자동 매핑  
	@RequestMapping(value="/edit", method=GET)
	public String redirectToEntryForm(@PathVariable int customerId, Model model) throws DataNotFoundException {
		Customer customer = customerService.findById(customerId);
		//Model에 editCustomer를 키값으로 하는 Customer 오브젝트 추가
		//@SessionAttribute에서 지정한 editCustomer와 이름이 같으므로 session 스코프로 설정
		model.addAttribute("editCustomer", customer);
		return "redirect:enter";
	}
	
	//enter 화면에 대해서 view 반환 메소드가 2개인 이유
	//  검증 후 다시 입력화면으로 돌아갔을때 만약 redirectToEntryForm()에서 전부 처리한다면
	//  Customer를 다시 생성하는 불필요 과정을 거침 
	@RequestMapping(value="/enter", method=GET) 
	public String showEntryForm() {
		return "customer/edit/enter";
	}
	
	/**
	 * 
	@RequestMapping(value="/enter", params="_event_proceed", method=POST)
	enter.jsp에서 전달받은 params가 _event_proceed 인 경우에 실행
	
	
	@ModelAttribute("이름") ClassType Object
	  Model에 저장된 ModelAttribute, 자동으로 request에 저장되어 jsp에서 사용 가능
	  화면에서 넘어온 값들이 지정된 ClassType과 일치하는 파라미터인 경우 자동으로 ClassType 객체와 바인딩
	  이를 위해서 ClassType의 Class는 기본 생성자 또는 인수가 없는 생성자를 구현해야 함
	  
	@SettionAttribute("이름")에서 지정한 이름과 동일하게 하여 Session에 Model의 ModelAttribute를 저장하여 여러 페이지에서 사용 가능
	@SettionAttribute("editCustomer")
	@ModelAttribute("editCustomer") Customer customer
	sessionAttribute, ModelAttribute는 이름 동일
	
	@Valid는 @ModelAttribute에 의해서 데이터 바인딩 처리가 종료된 이후 자동으로 검증 처리 실행. 검증	결과를 erros 인수에 설정
		
	Errors errors 오브젝트는 오류 발생시 입력 화면으로 이동에 사용
	오류가 발생 시 Erros 오브젝트에 저장되어 오류 발생 여부 확인
	대상이 되는 @ModelAttribute 뒤에 반드시 위치 필요!!
	
	* 
	 */
	@RequestMapping(value="/enter", params="_event_proceed", method=POST)
	public String verify(@Valid @ModelAttribute("editCustomer") Customer customer, Errors errors) {
		if(errors.hasErrors()) 
			return "customer/edit/enter";
		
		return "redirect:review";
	}
	
	@RequestMapping(value="/review", method=GET)
	public String showReview() {
		return "customer/edit/review";
	}
	
	@RequestMapping(value="/review", params="_event_revise", method=POST)
	public String revise() {
		return "redirect:enter";
	}
	
	@RequestMapping(value="/review", params="_event_confirmed", method=POST)
	public String edit(@ModelAttribute("editCustomer") Customer customer) throws DataNotFoundException {
		customerService.update(customer);
		return "redirect:edited";
	}
	
	@RequestMapping(value="/edited", method=GET)
	public String showEdited(SessionStatus sessionStatus) {
		//현재 클래스의 @SessionAttribute(이름)의 ModelAttribute만 삭제
		//이름이 같은 다른 클래스에서 지정한 ModelAttribute도 세션에서 삭제
		sessionStatus.setComplete();
		return "customer/edit/edited";
	}
	
	//flash Scope를 사용. request와 같은 기능 차이점은 redirect시 상태가 유지됨
	//화면이 표시되면 flash Scope는 초기화
	//session Scope 처럼 명시적으로 ModelAttribute 삭제할 필요 없음.
/*	@RequestMapping(value="/review", params="_event_confirmed", method=POST)
	public String edit(@ModelAttribute("editCustomer") Customer customer, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) throws DataNotFoundException {
		customerService.update(customer);
		sessionStatus.setComplete();
		redirectAttributes.addFlashAttribute("editCustomer", customer);
		return "redirect:/consumer";
	}*/
}
