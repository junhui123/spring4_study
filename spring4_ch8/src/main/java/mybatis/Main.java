package mybatis;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mybatis.business.domain.Pet;
import mybatis.business.service.PetDao;
import mybatis.config.DataSourceConfig;
import mybatis.config.MyBatisConfig;

public class Main {
	public static void main(String[] args) {
		//ApplicationContext ctx = new AnnotationConfigApplicationContext(DataSourceConfig.class, MyBatisConfig.class);
		ApplicationContext ctx = new ClassPathXmlApplicationContext("mybatis/config/spring-mybatis.xml");
		
 		PetDao petDao = ctx.getBean(PetDao.class);
		
 		Pet p = petDao.findById(1);
		System.out.println(p.toString());
		
		List<Pet> list = petDao.findAll();
		for(Pet pet : list) {
			System.out.println(pet);
		}
		
	}
}
