package sample.customer.config;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import sample.Exception.BarException;
import sample.Exception.BazException;
import sample.Exception.FooException;
import sample.Exception.SystemException;


@Configuration
@EnableWebMvc
@ComponentScan("sample.customer.web.controller")
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resource");
		registry.addResourceHandler("/image/**").addResourceLocations("/WEB-INF/image");
		registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css");
		registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js");
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.jsp("/WEB-INF/views/", "*.jsp");
	}

	@Autowired
	private MessageSource messageSource;

	@Override
	public Validator getValidator() {
		LocalValidatorFactoryBean validatorBean = new LocalValidatorFactoryBean();
		validatorBean.setValidationMessageSource(messageSource);
		return validatorBean;
	}
	
	//공통 예외 처리 정의
	@Bean
	public SimpleMappingExceptionResolver exceptionResolver() {
		Properties prop = new Properties();
		prop.setProperty(FooException.class.getName(), "error/foo");
		prop.setProperty(BarException.class.getName(), "error/bar");
		prop.setProperty(BazException.class.getName(), "error/baz");
		prop.setProperty(SystemException.class.getName(), "error/system");
		prop.setProperty(Exception.class.getName(), "error/exception");
		
		SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
		resolver.setExceptionMappings(prop);
		return resolver;
	}

	//HttpMessageConverter
	// HTTP Request/Response를 Java Object로 변환할때 사용
	// REST API 처리에 사용
	// HTTP 요청과 HTTP 응답의 보디 부분에 XML/JSON 형식 설정 가능하게 해줌
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new Jaxb2RootElementHttpMessageConverter());
	}
}
