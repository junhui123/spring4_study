package transaction.business.dao;

import transaction.business.domain.Pet;

public interface PetDao {
    void updatePet(Pet pet);
    void findByOwnerName(String owername);
}
