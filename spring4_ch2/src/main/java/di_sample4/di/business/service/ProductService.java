package di_sample4.di.business.service;

import di_sample4.di.business.domain.Product;

public interface ProductService {
	void addProduct(Product product);
	Product findByProductName(String name);
}
