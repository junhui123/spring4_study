package sample.Exception;

public class BazException extends Exception {
	private static final long serialVersionUID = 3614922944608509308L;

	public BazException() {
	}

	public BazException(String msg) {
		super(msg);
	}

	public BazException(Throwable th) {
		super(th);
	}

	public BazException(String msg, Throwable th) {
		super(msg, th);
	}
}
