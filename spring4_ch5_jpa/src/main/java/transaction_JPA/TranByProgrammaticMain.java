package transaction_JPA;

import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import transaction_JPA.business.domain.Pet;
import transaction_JPA.business.service.PetService;


public class TranByProgrammaticMain {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("transaction_JPA/config/spring-tranJPAByProgrammatic.xml");
		PetService service = ctx.getBean(PetService.class);
		List<Pet> pets = service.findPetByOwner("홍길동");
		for(Pet p : pets) {
			System.out.println(p.getPetId());
			System.out.println(p.getPetName());
			System.out.println(p.getOwnerName());
			System.out.println(p.getPrice());
			System.out.println(p.getBirthDate());
		}
		
		Pet pet = new Pet();
		pet.setPetId(1);
		pet.setPetName("나비");
		pet.setOwnerName("홍길동");
		pet.setBirthDate(new Date());
		service.updatePetJPA(60000, pet);
		//service.updatePet(60000, pet);
		
		pets = service.findPetByOwner("홍길동");
		for(Pet p : pets) {
			System.out.println(p.getPetId());
			System.out.println(p.getPetName());
			System.out.println(p.getOwnerName());
			System.out.println(p.getPrice());
			System.out.println(p.getBirthDate());
		}
	}
	
}
