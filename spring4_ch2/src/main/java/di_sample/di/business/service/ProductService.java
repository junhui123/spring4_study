package di_sample.di.business.service;

import di_sample.di.business.domain.Product;

public interface ProductService {
	void addProduct(Product product);
	Product findByProductName(String name);
}
