package transaction;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import transaction.business.domain.Pet;
import transaction.business.service.PetService;

public class TranByAnnotationMain {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("transaction/config/spring-tranByAnnotation.xml");
		PetService petService = ctx.getBean(PetService.class);
		Pet pet = new Pet();
		pet.setPetId(1);
		pet.setPetName("나비");
		pet.setOwnerName("홍길동");
		pet.setPrice(1000000);
		pet.setBirthDate(new Date());

		petService.updatePet(pet);
		
		petService.findPetByOwner("홍길동");
	}
}
