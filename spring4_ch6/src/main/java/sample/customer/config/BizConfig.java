package sample.customer.config;

import javax.validation.Validator;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@ComponentScan("sample.customer.biz.service")
public class BizConfig {
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:/META-INF/messages");
		return messageSource;
	}
	
	//@EnableWebMvc을 통해 Validator Bean 자동으로 등록되므로
	//Bean을 이중등록 하지 않기 위해 사용하지 않음
	//@EnableWebMvc는 MVC 설정 클래스에 적용됨
/*	@Bean
	public Validator globalValidator(MessageSource messageSource) {
		LocalValidatorFactoryBean validatorBean = new LocalValidatorFactoryBean();
		validatorBean.setValidationMessageSource(messageSource);
		return validatorBean;
	}*/
}
