package transaction.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import transaction.business.dao.PetDao;
import transaction.business.domain.Pet;

@Repository
public class PetDaoSpringJdbc implements PetDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public void updatePet(Pet pet) {
        jdbcTemplate.update(
            "UPDATE PET SET PET_NAME=?, OWNER_NAME=?, PRICE=?, BIRTH_DATE=? WHERE PET_ID=?"
            , pet.getPetName(), pet.getOwnerName(), pet.getPrice(), pet.getBirthDate(), pet.getPetId());
    }
    
	@Override
	public void findByOwnerName(String owername) {
		 List<Pet> pets = jdbcTemplate.query(
		            "select * from Pet where OWNER_NAME=?"
		            , new RowMapper<Pet>() {
						@Override
						public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
		                    Pet p = new Pet();
		                    p.setPetId(rs.getInt("PET_ID"));
		                    p.setPetName(rs.getString("PET_NAME"));
		                    p.setOwnerName(rs.getString("OWNER_NAME"));
		                    p.setPrice(rs.getInt("PRICE"));
		                    p.setBirthDate(rs.getDate("BIRTH_DATE"));
		                    return p;
						}
		            }, owername);	
		 
		for (Pet p : pets) {
			System.out.println(p.getPetId());
			System.out.println(p.getPetName());
			System.out.println(p.getOwnerName());
			System.out.println(p.getPrice());
			System.out.println(p.getBirthDate());
		}
	}
}
