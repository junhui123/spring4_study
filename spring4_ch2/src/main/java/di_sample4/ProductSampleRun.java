package di_sample4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import di_sample4.config.AppConfig;
import di_sample4.di.business.domain.Product;
import di_sample4.di.business.service.ProductService;

public class ProductSampleRun {
	public static void main(String[] args) {
		ProductSampleRun run = new ProductSampleRun();
		run.execute();
	}
	
	public void execute() {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		ProductService productService = ctx.getBean(ProductService.class);
		productService.addProduct(new Product("공책", 100));
		Product product = productService.findByProductName("공책");
		System.out.println(product);
		//context 닫기
		((AnnotationConfigApplicationContext)ctx).close(); 
	}
}
