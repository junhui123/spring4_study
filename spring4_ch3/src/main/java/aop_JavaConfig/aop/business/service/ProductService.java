package aop_JavaConfig.aop.business.service;

import aop_JavaConfig.aop.business.domain.Product;

public interface ProductService {
	void addProduct(Product product);
	Product findByProductName(String name);
}