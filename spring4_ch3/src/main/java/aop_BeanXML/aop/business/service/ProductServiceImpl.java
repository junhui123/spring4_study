package aop_BeanXML.aop.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import aop_BeanXML.aop.business.domain.Product;

@Component
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;

	public void addProduct(Product product) {
		productDao.addProduct(product);
	}

	public Product findByProductName(String name) {
		return productDao.findProduct(name);
	}

	@Override
	public Product findProductThrowsEx() {
		throw new RuntimeException();
	}
}