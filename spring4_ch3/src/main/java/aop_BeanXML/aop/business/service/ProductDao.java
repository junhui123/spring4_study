package aop_BeanXML.aop.business.service;

import aop_BeanXML.aop.business.domain.Product;

public interface ProductDao {
	void addProduct(Product product);

	Product findProduct(String name);
}
