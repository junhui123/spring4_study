package aop_JavaConfig.aop.business.service;

import aop_JavaConfig.aop.business.domain.Product;

public interface ProductDao {
	void addProduct(Product product);

	Product findProduct(String name);
}
