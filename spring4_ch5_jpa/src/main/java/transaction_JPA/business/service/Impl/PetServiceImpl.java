package transaction_JPA.business.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import transaction_JPA.business.domain.Pet;
import transaction_JPA.business.exception.BussinessException;
import transaction_JPA.business.service.PetService;
import transaction_JPA.dao.PetDao;

@Service
public class PetServiceImpl implements PetService {
	@Autowired
	private PetDao petDao;
	@Autowired
	private PlatformTransactionManager txManager;
	public void updatePetJPA(int price, Pet pet) {
		//트랜잭션 시작
		TransactionStatus status = txManager.getTransaction(null);
		petDao.updatePetPrice(price, pet.getPetName());
        //트랜잭션 커밋
		txManager.commit(status);
	}
	@Override
	public List<Pet> findPetByOwner(String name) {
		return petDao.findByOwnerName(name);
	}
}
