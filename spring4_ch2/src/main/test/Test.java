import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import di_sample.di.business.domain.Product;
import di_sample.di.business.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
//"file:WebContent/WEB-INF/spring/root-context.xml"
@ContextConfiguration(locations = {"classpath:/di_sample/config/applicationContext.xml"})
public class Test {

	@Autowired
	private ApplicationContext ctx;

	@org.junit.Test
	public void test() {
		ProductService productService = ctx.getBean(ProductService.class);
		productService.addProduct(new Product("공책", 100));
		assertNotNull(productService);
		
		Product product = productService.findByProductName("공책");
		System.out.println(product);
		
		assertNotNull(product);
	}

}
