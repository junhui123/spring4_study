package di_sample3.di.dataaccess;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import di_sample3.di.business.domain.Product;
import di_sample3.di.business.service.ProductDao;

public class ProductDaoImpl implements ProductDao {
	private Map<String, Product> storage = new HashMap<>();
	
	@Override
	public void addProduct(Product product) {
		storage.put(product.getName(), product);
	}

	@Override
	public Product findByProductName(String name) {
		return storage.get(name);
	}

}
