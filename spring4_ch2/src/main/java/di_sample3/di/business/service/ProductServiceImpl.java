package di_sample3.di.business.service;

import di_sample3.di.business.domain.Product;
import di_sample3.di.business.service.ProductDao;
import di_sample3.di.business.service.ProductService;

public class ProductServiceImpl implements ProductService {

	private ProductDao productDao;
	
	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}
	
	@Override
	public void addProduct(Product product) {
		productDao.addProduct(product);
	}

	@Override
	public Product findByProductName(String name) {
		return productDao.findByProductName(name);
	}
}
