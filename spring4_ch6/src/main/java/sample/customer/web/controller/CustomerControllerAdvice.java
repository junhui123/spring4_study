package sample.customer.web.controller;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

import sample.customer.biz.service.DataNotFoundException;

@Component
//공통 기능에 대한 모듈화 (InitBinder, 공통 예외처리, 공통 ModelAttribute), 여러 패키지/특정 클래스 적용 가능
@ControllerAdvice("sample.customer.web.controller")
//@ControllerAdvice(assignableTypes= {CustomerEditController.class, CustomerListController.class})
public class CustomerControllerAdvice {
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@ExceptionHandler(DataNotFoundException.class)
	public String handleException() {
		return "customer/notfound";
	}
	
	@ExceptionHandler()
	public String handleException(Exception e) {
		return "error";
	}
	
}
