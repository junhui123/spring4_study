package MessageSource;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ResourceBundleMessageSource;

import MessageSource.config.MessageSourceConfiguration;

public class MessageSource_JavaConfiguration {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(MessageSourceConfiguration.class);
		ResourceBundleMessageSource source = context.getBean(ResourceBundleMessageSource.class);
		source.setDefaultEncoding("utf-8");
		source.setBasenames("MessageSource.resources.messages.message");
		String msg_default = source.getMessage("geeting", null, Locale.getDefault());
		System.out.println(msg_default);
		String msg_ko = source.getMessage("geeting", null, Locale.KOREA);
		System.out.println(msg_ko);
		String msg_en = source.getMessage("geeting", null, Locale.ENGLISH);
		System.out.println(msg_en);
	}
}

