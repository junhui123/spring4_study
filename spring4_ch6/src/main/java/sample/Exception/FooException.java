package sample.Exception;

public class FooException extends Exception {
	private static final long serialVersionUID = 3614922944608509308L;

	public FooException() {
	}

	public FooException(String msg) {
		super(msg);
	}

	public FooException(Throwable th) {
		super(th);
	}

	public FooException(String msg, Throwable th) {
		super(msg, th);
	}
}
