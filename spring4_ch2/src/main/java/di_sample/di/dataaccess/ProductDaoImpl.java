package di_sample.di.dataaccess;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import di_sample.di.business.domain.Product;
import di_sample.di.business.service.ProductDao;

/**
 *@Component
 * DI 컨테이너가 관리하는 인젝션을 위한 인스턴스 설정에 사용
 *
 *@Primary
 * 상위 인터페이스 구현이 여러개인경우 디폴트로 설정하여 인젝션
 * 인젝션은 하나의 구현에 대해서만 가능. 
 */
@Component
@Primary  
public class ProductDaoImpl implements ProductDao {
	private Map<String, Product> storage = new HashMap<>();
	
	@Override
	public void addProduct(Product product) {
		storage.put(product.getName(), product);
	}

	@Override
	public Product findByProductName(String name) {
		return storage.get(name);
	}

}
