package mybatis.dataaccess;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mybatis.business.domain.Pet;
import mybatis.business.service.PetDao;

//Mapper 구현
@Repository
public class MyBatisPetDao implements PetDao {

	@Autowired
	private SqlSession ss;
	
	@Override
	public Pet findById(int petId) {
		return ss.selectOne("mybatis.business.service.PetDao.findById", petId);
	}

	@Override
	public List<Pet> findAll() {
		return ss.selectList("mybatis.business.service.PetDao.findAll");
	}

}
