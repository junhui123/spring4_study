package sample.customer.web.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import sample.customer.biz.domain.Customer;
import sample.customer.biz.service.CustomerService;
import sample.customer.biz.service.DataNotFoundException;

//REST 전용 컨트롤러. @ResponseBody 어노테이션 생략 가능
@RestController
@RequestMapping("/api/customer")
public class CustomerRestController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(method=POST)
	//어노테이션 지정하지 않아도 기본 코드 200 OK
	@ResponseStatus(HttpStatus.OK) 
	//@ResponseBody
	// @RestController인 경우 생략 가능, 결과를 http response body에 삽입할 것을 설정 
	// 인수의 @ResponseBody는 Http Request body를 받기 위한 설정.
	//@ResponseBody로 설정된 인수에 HttpMessageConverter 변환 결과 설정
	public String register(@RequestBody Customer customer) {
		customerService.register(customer);
		//@Responsebody 
		//return을 통해 http response body에 삽입할 값 설정 
		return "OK";
	}
	
	
	//ResponseEntity = Http 응답을 나타내는 헤더와 바디를 구성하는 클래스
	//ResponseEntity 클래스를 통해 응답 코드, 헤더, 바디 설정 하므로 @ResponseBody, @ResponseStatus 필요 없음
	@RequestMapping(value="/{customerId}", method=GET)
	public ResponseEntity<Customer> findById(@PathVariable int customerId) throws DataNotFoundException {
		Customer customer = customerService.findById(customerId);
		return ResponseEntity.ok()
							 .header("My-Header", "MyHeaderValue")
							 .contentType(new MediaType("text", "xml", Charset.forName("UTF-8")))
							 .body(customer);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleException(DataNotFoundException e) {
		return "customer is not found";
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleException(Exception e) {
		return "server error";
	}
}