package di_sample2;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import di_sample.di.business.domain.Product;
import di_sample.di.business.service.ProductService;

public class ProductSampleRun {
	public static void main(String[] args) {
		ProductSampleRun productSampleRun = new ProductSampleRun();
		productSampleRun.execute();
	}

	private void execute() {
		BeanFactory ctx = new ClassPathXmlApplicationContext("/di_sample2/config/applicationContext.xml");
		ProductService productService = ctx.getBean(ProductService.class);
		productService.addProduct(new Product("공책", 100));
		
		Product product = productService.findByProductName("공책");
		System.out.println(product);
	}
}
