package mybatis.business.domain;

import java.util.Date;

public class Pet {
	private Integer petId;
	private String petName;
	private Integer price;
	private Date birthDate;

	public Integer getPetId() {
		return petId;
	}

	public void setPetId(Integer petId) {
		this.petId = petId;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Pet ID = " + petId);
		sb.append(" Pet Name = " + petName);
		sb.append(" Pet Price = " + price);
		sb.append(" Pet BirthDate = " + birthDate);
		return sb.toString();
	}
}