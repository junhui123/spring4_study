package MessageSource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessageSourceConfiguration {
	@Bean
	public ResourceBundleMessageSource messageService() {
		return new ResourceBundleMessageSource();
	}
}
