package sample.customer.web.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import sample.customer.biz.domain.Customer;
import sample.customer.biz.service.CustomerService;
import sample.customer.biz.service.DataNotFoundException;

@Controller
public class CustomerListController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value="/", method=GET)
	public String home() {
		return "forward:/customer";
	}
	
	@RequestMapping(value="/customer", method=GET)
	public String showAllCustomers(Model model) {
		List<Customer> customers = customerService.findAll();
		model.addAttribute("customers",  customers);
		return "customer/list";
	}
	
	//@PathVariable = {}안의 지정된 변수명(변동이 있는 값)과 메소드 인수명을 같게하여 변동이 있는 값을 자동 매핑  
	@RequestMapping(value="/customer/{customerId}", method=GET)
	public String showCustomerDetail(@PathVariable int customerId, Model model) throws DataNotFoundException {
		Customer customer = customerService.findById(customerId);
		model.addAttribute("customer", customer);
		return "customer/detail";
	}
	
	//DataNotFoundException은 Exception의 자식 클래스. 우선순위 높음
	//@ExceptionHandler({DataNotFoundException.class, NullPointerException.class})
	@ExceptionHandler(DataNotFoundException.class)
	public String handleException() {
		return "customer/notfound";
	}
	
	//메소드에 익셉션 타입 인수 지정 == @ExceptionHandler(DataNotFoundException.class) 
/*	@ExceptionHandler()
	public String handleException(DataNotFoundException e) {
		return "customer/notfound";
	}*/
	
	@ExceptionHandler
	public String handleException(Exception e) {
		return "error/system";
	}
}
