package jpa.sample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import jpa.sample.business.domain.Pet;
import jpa.sample.business.service.PetDao;
import jpa.sample.config.AppConfig;
import jpa.sample.config.DataSourceConfig;
import jpa.sample.config.JpaConfig;

public class Main {
	public static void main(String[] args) {
		
    	//Spring 컨테이너 생성        
    	//JavaConfig로 Bean을 정의한 경우
        //ApplicationContext ctx = new AnnotationConfigApplicationContext(DataSourceConfig.class, AppConfig.class, JpaConfig.class);
		
		//Spring 컨테이너 생성      
		//Bean 설정 XML을 통한 ApplicationContext 생성
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/jpa/sample/config/spring-jpa.xml");
		
		PetDao dao = ctx.getBean(PetDao.class);
		
		//Spring Data JPA가 자동생성한 메소드
		Pet pet = dao.findOne(1);
		System.out.println(pet.getPetName());
		
		//명명규칙에 의한 메소드 PetName=="발바리"&&Price<=5000
		List<Pet> list = dao.findByPetNameAndPriceLessThanEqual("발바리", 5000);
		System.out.println(list.size());
		
		//JPQL 쿼리로 정의한 메소드 호출
		list = dao.findByOwnerName("홍길동");
		System.out.println(list.size());
		
		//트랜잭션
		PlatformTransactionManager t = ctx.getBean(PlatformTransactionManager.class);
		
		//트랜잭션 시작
		TransactionStatus s = t.getTransaction(null);
		
		int count = dao.updatePetPrice(10000000, "발바리5");
        System.out.println("count="+count);

        //트랜잭션 커밋
        t.commit(s);
        
        list = dao.findAll();
        for(Pet p : list) {
        	System.out.println(p.getPrice());
        }
        
        dao.foo();
        
        int price = dao.getPetPrice(10);
        System.out.println(price);
        
	}
	
	public static void cretae() {
		//Persistence.xml에서  <persistence-unit>에 대한 설정이 필요
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		Pet pet = new Pet();
		pet.setPetId(1);
		pet.setPetName("순돌이");
		pet.setPrice(15000);
		try {
			pet.setBirthData(new SimpleDateFormat("yyyyMMdd").parse("20150101"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		em.persist(pet);
		
		Pet pet10 = em.find(Pet.class, 10);
		em.remove(pet10);
		
		Pet pet11 = em.find(Pet.class, 11);
		pet11.setPetName("복실이");
		
		tx.commit();
		
		List<Pet> petList = em.createQuery("select p from Pet p where p.price < ?1").setParameter(1, 10000).getResultList();
		for(Pet p : petList) {
			System.out.println(p.getPetName()+" "+p.getPetId());
		}
	}
}
