package aop_annotation.aop.business.service;

import aop_annotation.aop.business.domain.Product;

public interface ProductDao {
	void addProduct(Product product);
	Product findProduct(String name);
}
