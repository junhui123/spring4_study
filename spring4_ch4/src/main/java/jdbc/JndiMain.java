package jdbc;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import jdbc.sample.config.JndiConfig;
import jdbc.sample.config.TemplateConfig;

public class JndiMain {
	public static void main(String[] args) throws Exception {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
        ds.setUrl("jdbc:hsqldb:mem:sample");
        ds.setUsername("sa");
        ds.setPassword("");
        
        datainitialize(ds);
        
        //Spring-test가 제공하는 JNDI 구현(테스트 용)을 사용해서, 네이밍컨텍스트를 생성
        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
        //BasicDataSource ds를 바인딩하여 JndiConfig에서 사용 가능 
        builder.bind("jdbc/MyDataSource", ds);
        builder.activate();
        
        //Bean정의 파일을 사용해서, JNDI로 테이터 소스 취득
        //ApplicationContext ctx = new ClassPathXmlApplicationContext("jdbc/sample/config/spring-jndi.xml");
        
        //JavaConfig를 사용해서, JNDI로 데이터 소스 취득
        ApplicationContext ctx = new AnnotationConfigApplicationContext(JndiConfig.class, TemplateConfig.class);
        
        JdbcTemplate jdbcTemplate = ctx.getBean(JdbcTemplate.class);
                
        int count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM PET", Integer.class);
        System.out.println(count);
        
        
    }
    
    private static void datainitialize(DataSource ds) {
        ResourceDatabasePopulator p = new ResourceDatabasePopulator();
        p.addScripts(
                new ClassPathResource("/script/table.sql"),
                new ClassPathResource("/script/data.sql")
                );
        p.execute(ds);
        p = new ResourceDatabasePopulator();
        p.addScript(
                new ClassPathResource("/script/proc.sql")
                );
        p.setSeparator("/");
        p.execute(ds);
        
    }
}