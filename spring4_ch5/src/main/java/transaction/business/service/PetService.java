package transaction.business.service;

import transaction.business.domain.Pet;

public interface PetService {
	void updatePet(Pet pet);
	void updatePetProgrammaticTransaction(Pet pet);
	void updatePetProgrammaticTransaction2(Pet pet);
	void findPetByOwner(String name);
}
