package aop_BeanXML.aop.business.service;

import aop_BeanXML.aop.business.domain.Product;

public interface ProductService {
	void addProduct(Product product);
	Product findByProductName(String name);
	Product findProductThrowsEx();
}
