package aop_annotation.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import aop_annotation.aop.business.domain.Product;

@Aspect
@Component
public class MyFirstAspect {
	
	@Before("execution(* findProduct(String))")
	public void before() {
		System.out.println("Hello Before ** 메소드가 호출되기 전");
	}
	
	@After("execution(* findProduct(String))")
	public void after() {
		System.out.println("Hello After ** 메소드 호출 후");
	}
	
	@AfterReturning(value="execution(* findProduct(String))", returning="product")
	public void afterReturning(Product product) {
		//메소드 호출이 예외 없이 정상 종료
		System.out.println("Hello AfterReturning ** 메소드 호출 후 정상종료");
	}
	
	@Around(value="execution(* findProduct(String))")
	public Product around(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("Hello Around! before ** 메소드 호출 하기 전");
		
		//Aspect 대상인 메소드 호출을 위함
		Product p = (Product) pjp.proceed();
		System.out.println("Hello Around! after ** 메소드 호출 후");
		return p;
	}
	
	@AfterThrowing(value="execution(* findByProductNameThrowEx(String))", throwing="ex")
	public void afterThrowing1(Throwable ex) {
		System.out.println("Hello Throwing1 ** 메서드 호출 예외 발생(findByProductNameThrowEx)");
	}
	
	@AfterThrowing(value="execution(* findByProductName(String))", throwing="ex")
	public void afterThrowing2(Throwable ex) {
		System.out.println("Hello Throwing2 ** 메서드 호출 예외 발생 (findByProductName)");
	}
}
