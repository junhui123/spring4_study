package aop_annotation.aop.business.service;

import aop_annotation.aop.business.domain.Product;

public interface ProductService {
	void addProduct(Product product);
	Product findByProductName(String name) throws Exception;
	Product findByProductNameThrowEx(String string) throws Exception;
}
