package aop_annotation.aop.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import aop_annotation.aop.business.domain.Product;

@Component
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;

	public void addProduct(Product product) {
		productDao.addProduct(product);
	}

	public Product findByProductName(String name) throws Exception  {
		if(name.equals(""))
			throw new RuntimeException("findByProductName - name is blank");
		
		
		return productDao.findProduct(name);
	}
	
	public Product findByProductNameThrowEx(String name) throws Exception {
		throw new RuntimeException("findByProductNameThrowEx");
	}
}