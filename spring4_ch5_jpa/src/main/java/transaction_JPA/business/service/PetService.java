package transaction_JPA.business.service;

import java.util.List;

import transaction_JPA.business.domain.Pet;


public interface PetService {
	List<Pet> findPetByOwner(String name);
	void updatePetJPA(int price, Pet pet);
}
