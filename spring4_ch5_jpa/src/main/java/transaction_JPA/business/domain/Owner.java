package transaction_JPA.business.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Owner {
	@Id
	private Integer ownerId;
	private String ownerName;

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
}
