package di_sample2.di.business.service;

import di_sample2.di.business.domain.Product;

public interface ProductDao {
	void addProduct(Product product);
	Product findByProductName(String name);
}
