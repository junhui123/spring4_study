package di_sample4.di.dataaccess;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import di_sample4.di.business.domain.Product;
import di_sample4.di.business.service.ProductDao;

public class ProductDaoImpl implements ProductDao {
	private Map<String, Product> storage = new HashMap<>();
	
	@Override
	public void addProduct(Product product) {
		storage.put(product.getName(), product);
	}

	@Override
	public Product findByProductName(String name) {
		return storage.get(name);
	}
	
	@PostConstruct
	public void start() {
		System.out.println("@PostConstruct : ProductDaoImpl");
	}
	
	@PreDestroy
	public void stop() {
		System.out.println("@PreDestory : ProductDaoImpl");
	}
}
