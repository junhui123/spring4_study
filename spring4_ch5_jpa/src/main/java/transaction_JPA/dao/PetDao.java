package transaction_JPA.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import transaction_JPA.business.domain.Pet;


@Repository
public interface PetDao extends JpaRepository<Pet, Integer>{
	@Query("select p from Pet p where p.ownerName = :ownerName")
	List<Pet> findByOwnerName(@Param("ownerName") String ownerName);
	
	@Modifying
	@Query("update Pet p set p.price = ?1 where p.petName = ?2")
	void updatePetPrice(Integer price, String petName);
}
