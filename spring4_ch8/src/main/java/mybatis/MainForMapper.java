package mybatis;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mybatis.business.domain.Pet;
import mybatis.business.service.PetDao;
import mybatis.config.DataSourceConfig;
import mybatis.config.MyBatisConfigForMapper;


public class MainForMapper {

    public static void main(String[] args) {
    	//Spring의 컨테이너를 생성        
    	//JavaConfig로 Bean을 정의한 경우
        //ApplicationContext ctx = new AnnotationConfigApplicationContext(DataSourceConfig.class, MyBatisConfigForMapper.class);

    	//Spring의 컨테이너를 생성  
    	//XML로 Bean을 정의한 경우
        ApplicationContext ctx = new ClassPathXmlApplicationContext("mybatis/config/spring-mybatis-mapper.xml");
        
        PetDao dao = ctx.getBean(PetDao.class);
        
        List<Pet> list = dao.findAll();
        for(Pet pet : list) {
        	System.out.println(pet.toString());
        }
        
        Pet p = dao.findById(2);
        System.out.println(p.toString());
        
        
    }

}
