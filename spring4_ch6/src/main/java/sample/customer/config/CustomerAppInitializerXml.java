package sample.customer.config;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractDispatcherServletInitializer;

public class CustomerAppInitializerXml {}

/*public class CustomerAppInitializerXml extends AbstractDispatcherServletInitializer {

	@Override
	protected WebApplicationContext createServletApplicationContext() {
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocation("classpath:/META-INF/spring/beans-biz.xml");
		return ctx;
	}

	@Override
	protected WebApplicationContext createRootApplicationContext() {
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocation("classpath:/META-INF/spring/beans-webmvc.xml");
		return ctx;
	}
	
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter f = new CharacterEncodingFilter();
		f.setEncoding("UTF-8");
		f.setForceEncoding(true);
		
		return new Filter[] {f};
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.setInitParameter("defaultHtmlEscape", "true");
	}
}*/
