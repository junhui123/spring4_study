package aop_annotation;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import aop_annotation.aop.business.domain.Product;
import aop_annotation.aop.business.service.ProductService;

public class ProductSampleRun {

	public static void main(String[] args) {
		ProductSampleRun productSampleRun = new ProductSampleRun();
		productSampleRun.execute();
		productSampleRun.executeThrowEx1();
		productSampleRun.executeThrowEx2();
	}

	@SuppressWarnings("resource")
	public void execute() {
		ProductService productService = getService();
		productService.addProduct(new Product("공책", 100));

		Product product;
		try {
			product = productService.findByProductName("공책");
			System.out.println(product);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("resource")
	public void executeThrowEx1() {
		ProductService productService = getService();
		try {
			productService.findByProductName("");
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
	
	@SuppressWarnings("resource")
	public void executeThrowEx2() {
		ProductService productService = getService();
		try {
			productService.findByProductNameThrowEx("");
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
	
	public ProductService getService() {
		// BeanFactory는 ApplicationContext에 적어도 괜찮습니다.
		BeanFactory ctx = new ClassPathXmlApplicationContext("/aop_annotation/config/applicationContext.xml");
		ProductService productService = ctx.getBean(ProductService.class);
		return productService;
	}
}