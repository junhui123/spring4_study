package jpa.sample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jpa.sample.dataacess.PetDaoImpl;

@Configuration
public class AppConfig {

	@Bean
	public PetDaoImpl petDaoImpl() {
		return new PetDaoImpl();
	}
	
}
